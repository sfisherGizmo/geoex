<?php

class getResults {

	protected static $user = 'stephen@squibble-fish.com';
	protected static $md5 = 'b1c7e8fcb990fc07c25194f6eacd8903';
	protected static $sid = '1161513';
	protected static $v = 'v4';

	// map to convert ratings from num to string
	var $rating_map = array(
		'1'		=> 'Poor',
		'2'		=> 'Marginal',
		'3'		=> 'Satisfactory',
		'4'		=> 'Very Good',
		'5'		=> 'Excellent',
		'N/A' 	=> 'N/A',
	);

	var $yes_no_map = array(
		'1' 	=> 'No',
		'2'		=> 'Yes',
		'3'		=> 'Exceeded Them',
	);

	/**
	 * Gets a single response from the api
	 *
	 * @param $rid response id to pull
	 */
	public static function getSingleResult( $rid ) {

		require_once dirname( dirname( __FILE__ ) ) . '/sgapiModel.php';

		$sg_api = new restapi;
		$ids = array(
			'survey' => self::$sid,
			'surveyresponse' => $rid
		);
		$sg_api->setup( self::$user, self::$md5, self::$v, $resultsperpage=1 );
		$response = $sg_api->get( "surveyresponse", $ids );

		return $response;
	}

	/**
	 * parses returned single response
	 *
	 * @param $obj json encoded response from api
	 */
	public function parseObj ( $obj ) {

		// header("Content-Type: text/plain; charset=ISO-8859-1");
		
		$data = array();
		// $guides = explode( ";", str_replace( "&#12287;", "'", $obj->{'[question(174)]'} ) );
		// $hotels = explode( ";", str_replace( "&#12287;", "'", $obj->{'[question(521)]'} ) );
		// $office_staff = explode( ";", str_replace( "&#12287;", "'", $obj->{'[question(549)]'} ) );
 		$guides = explode( ";", str_replace( "?", "'" ,utf8_decode( $obj->{'[question(174)]'} ) ) );
 		$hotels = explode( ";", str_replace( "?", "'", utf8_decode( $obj->{'[question(521)]'} ) ) );
 		$office_staff = explode( ";", str_replace( "?", "'", utf8_decode( $obj->{'[question(549)]'} ) ) );
 		// $this-rating_map : converts num to string 
 		$data = array(
 			'name' 			=> $obj->{'[question(87)]'},
 			'email'			=> $obj->{'[question(122)]'},
 			'expectations'	=> $this->yes_no_map[ $obj->{ '[question(15)]' } ],
 			'rate_trip'		=> $this->rating_map[ $obj->{ '[question(82)]' } ],
 			'recommend'		=> $obj->{'[question(489)]'},
 			'continue'		=> $obj->{'[question(178)]'},
 			'lead_comment' 	=> $obj->{'[question(32)]'},
 			'guide_comment'	=> $obj->{'[question(373)]'},
 			'staff_comment' => $obj->{'[question(554)]'},
 			'hotel_comment' => $obj->{'[question(227)]'},
 			'highlights'	=> $obj->{'[question(46)]'},
 			'improve'		=> $obj->{'[question(47)]'},
 			'advice'		=> $obj->{'[question(48)]'},
 			'why'			=> array(
 				$obj->{'[question(523)]'},
 				$obj->{'[question(524)]'},
 				$obj->{'[question(525)]'},
 			),
 			'why_comments'	=> $obj->{'[question(526)]'},
 			'when'			=> array(
 				$obj->{'[question(527)]'},
 				$obj->{'[question(528)]'},
 				$obj->{'[question(529)]'},
 			),
			'plan_trip'		=> str_replace( "?", "'", utf8_decode( $obj->{'[question(530)]'} ) ),
			'magazine'		=> array(
				utf8_decode( $obj->{'[question(532)]'} ),
				utf8_decode( $obj->{'[question(533)]'} ),
				utf8_decode( $obj->{'[question(534)]'} ),
			),
			'next_trip' 	=> array(
				$obj->{'[question(536)]'},
				$obj->{'[question(537)]'},
				$obj->{'[question(538)]'},
			),
			'conditional'	=> $obj->{'[question(1025)]'},
			'reference'		=> $obj->{'[question(1027)]'},
			'final'			=> $obj->{'[question(73)]'},
			'phone'			=> array(
				'contact'	=> $obj->{'[question(79)]'},
				'number'	=> $obj->{'[question(79), option("10064-other")]'},
			),
			'time'			=> $obj->{'[question(555)]'},
			'bad_comment'	=> $obj->{'[question(127)]'},
			'phone_bad'		=> array(
				'contact' 	=> $obj->{'[question(192)]'},
				'number'	=> $obj->{'[question(192), option("10079-other")]'},
			),
			'marketing'		=> array(
				'itinerary'	=> $obj->{'[question(57)]'},
				'grading'	=> $obj->{'[question(58)]'},
				'materials'	=> $obj->{'[question(59)]'},
				'website'	=> $obj->{'[question(60)]'},
				'catalog'	=> $obj->{'[question(61)]'},
				'survey'	=> $obj->{'[question(62)]'},
				'comments'	=> $obj->{'[comment(56)]'},
			),
 		);
		
		// $this-rating_map : converts num to string 
		$i = 0;
 		foreach ( $guides as $guide ) {
 			$data['guides_rating'][$guide] = $this->rating_map[ $obj->{ '[question(368), question_pipe("'.$guide.'")]' } ];
 			$data['hotel_rating'][$hotels[$i]] = $this->rating_map[ $obj->{ '[question(222), question_pipe("'.$hotels[$i].'")]' } ];
 			$data['staff_rating'][$office_staff[$i]] = $this->rating_map[ $obj->{ '[question(546), question_pipe("'.$office_staff[$i].'")]' } ];
 			$i++;
 		}

 		return $data;
	} 

	/**
	 *
	 *
	 */
	public static function output( $response ) {
		echo json_encode( $response );
	}

}

if ( !isset( $_REQUEST['id'] ) || $_REQUEST['id'] === '' ) {
	$response = array( 
		'result' 	=> false,
		'error' 	=> 'no data', 
	);
	getResults::output( $response );
	die();
}

$res = new getResults;
$obj = $res->getSingleResult( $_REQUEST['id'] );
$new = $res->parseObj( $obj );
//$res = getResults::getSingleResult( $_REQUEST['id'] );


?>