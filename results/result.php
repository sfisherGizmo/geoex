<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require 'sgapiController.php';

// echo "<pre>" . print_r($new , true) . "</pre>";

?>
<!DOCTYPE html>
<html>
	<head>
		<link href="assets/style.css" rel="stylesheet" type="text/css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<title>GeoEx Feedback Results</title>
	</head>
	<body>
		<div class="wrap" id="pg_one">
			<div class="intro-wrap">
				<h1 class="intro">1. Welcome</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Name</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php echo $new['name'] ?></span>
				</div>
				<div class="input">
					<span class="label">Email</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php echo $new['email'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_two">
			<div class="intro-wrap">
				<h1 class="intro">2. Your overall impression of your experience</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Did the trip live up to your expectations?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['expectations'] ?></span>
				</div>
				<div class="input">
					<span class="label">How would you rate the trip overall?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php echo $new['rate_trip'] ?></span>
				</div>
				<div class="input">
					<span class="label">Would you recommend Geoex to a friend?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php echo $new['recommend'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_three">
			<div class="intro-wrap">
				<h1 class="intro">3. Dissatisfied Client</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label"><strong>We are very sorry that the trip did not meet your expectations</strong>. <br />Would you like to continue with the survey?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['continue'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_four">
			<div class="intro-wrap">
				<h1 class="intro">4. Trip Leader</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Please rate your trip leader on their performance (Expertise, friendliness, ability to handle logistics, etc.)</span>
				</div>
				<div class="answer">
					<span class="ans-table">
						<table>
							<tr>
								<th>Guide</th><th>Rating</th>
							</tr>
						<?php   
							foreach ( $new['guides_rating'] as $guide => $rating ) { 
						?>
							<tr>
								<td><?php echo $guide ?></td><td><?php echo $rating ?></td>
							</tr>		
						<?php	
							}
						?>
						</table>
					</span>
				</div>
				<div class="input">
					<span class="label">Do you have comments about how your trip leader excelled or could improve?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['something'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_five">
			<div class="intro-wrap">
				<h1 class="intro">5. Guides & Field Staff</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Please rate your guides and field staff on their performance (Expertise, friendliness, ability to handle logistics, etc.):</span>
				</div>
				<div class="answer">
					<span class="ans-table">
						<table>
							<tr>
								<th>Guide</th><th>Rating</th>
							</tr>
						<?php   
							foreach ( $new['guides_rating'] as $guide => $rating ) { 
						?>
							<tr>
								<td><?php echo $guide ?></td><td><?php echo $rating ?></td>
							</tr>		
						<?php	
							}
						?>
						</table>
					</span>
				</div>
				<div class="input">
					<span class="label">We appreciate that you have traveled with a number of guides and other staff on your trip. Please feel free to provide us with individual details/ratings:</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['guide_comment'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_six">
			<div class="intro-wrap">
				<h1 class="intro">6. Office staff</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Please rate the GeoEx staff on their performance (Expertise, friendliness, ability to handle logistics, etc.) We recognize that you may not have worked directly with everyone listed here. Alternatively, if we've missed anyone, please feel free to add additional comments below.</span>
				</div>
				<div class="answer">
					<span class="ans-table">
						<table>
							<tr>
								<th>Staff</th><th>Rating</th>
							</tr>
						<?php   
							foreach ( $new['staff_rating'] as $staff => $rating ) { 
						?>
							<tr>
								<td><?php echo $staff ?></td><td><?php echo $rating ?></td>
							</tr>		
						<?php	
							}
						?>
						</table>
					</span>
				</div>
				<div class="input">
					<span class="label">Please feel free to provide us with further details:</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['staff_comment'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_eight">
			<div class="intro-wrap">
				<h1 class="intro">8. Hotels</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Please rate the overall quality of your hotels (Service, cleanliness, food, etc.)</span>
				</div>
				<div class="answer">
					<span class="ans-table">
						<table>
							<tr>
								<th>Hotel</th><th>Rating</th>
							</tr>
						<?php   
							foreach ( $new['hotel_rating'] as $hotel => $rating ) { 
						?>
							<tr>
								<td><?php echo $hotel ?></td><td><?php echo $rating ?></td>
							</tr>		
						<?php	
							}
						?>
						</table>
					</span>
				</div>
				<div class="input">
					<span class="label">We appreciate that you have stayed in multiple hotels/lodges on your trip. Please feel free to provide us with details on individual properties.</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['hotel_comment'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_nine">
			<div class="intro-wrap">
				<h1 class="intro">9. Highlights</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Please list three highlights from your trip.</span>
				</div>
				<div class="answer">
					<span class="ans-txt"><?php echo $new['highlights'] ?></span>
				</div>
				<div class="input">
					<span class="label">How could we improve this trip?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['improve'] ?></span>
				</div>
				<div class="input">
					<span class="label">What advice do you have for someone taking the same trip in the future?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['advice'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_ten">
			<div class="intro-wrap">
				<h1 class="intro">10. Marketing Questions</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Why did you choose GeoEx?</span>
				</div>
				<div class="answer">
					<span class="ans-list">
						<ul>
						<?php
							foreach ( $new['why'] as $why ) {
						?>
								<li><?php echo $why ?></li>		
						<?php
							}
						?>
						</ul>
					</span>
				</div>
				<div class="input">
					<span class="label">Comments:</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['why_comments'] ?></span>
				</div>
				<div class="input">
					<span class="label">When do you prefer to travel?</span>
				</div>
				<div class="answer">
					<span class="ans-list">
						<ul>
						<?php
							foreach ( $new['when'] as $when ) {
						?>
								<li><?php echo $when ?></li>		
						<?php
							}
						?>
						</ul>
					</span>
				</div>
				<div class="input">
					<span class="label">How early do you start planning for your trip?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['plan_trip'] ?></span>
				</div>
				<div class="input">
					<span class="label">What magazines or blogs are your favorite reads? Choose your top 3.</span>
				</div>
				<div class="answer">
					<span class="ans-list">
						<ul>
						<?php
							foreach ( $new['magazine'] as $magazine ) {
						?>
								<li><?php echo $magazine ?></li>		
						<?php
							}
						?>
						</ul>
					</span>
				</div>
				<div class="input">
					<span class="label">Where would you like to travel to next?</span>
				</div>
				<div class="answer">
					<span class="ans-list">
						<ul>
						<?php
							foreach ( $new['next_trip'] as $trip ) {
						?>
								<li><?php echo $trip ?></li>		
						<?php
							}
						?>
						</ul>
					</span>
				</div>
				<div class="input">
					<span class="label">May we use your comments in future trip promotional materials?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['conditional'] ?></span>
				</div>
				<div class="input">
					<span class="label">May we use you as a reference for clients interested in a trip similar to yours?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['reference'] ?></span>
				</div>
				<div class="input">
					<span class="label">Any final comments?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['final'] ?></span>
				</div>
				<div class="input">
					<span class="label">Please have someone call me (If you want someone to call you, check the radio button and add your phone: (999)999-9999</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['phone']['contact'] ?>&nbsp;:&nbsp;<?php echo $new['phone']['number'] ?></span>
				</div>
				<div class="input">
					<span class="label">Do you have time for one more page of questions?</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['time'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_elev">
			<div class="intro-wrap">
				<h1 class="intro">11. We are sorry that you were dissatisfied!</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Please describe the way in which the trip failed to meet your expectations.</span>
				</div>
				<div class="answer">
					<span class="ans-txt"><?php echo $new['bad_comment'] ?></span>
				</div>
				<div class="input">
					<span class="label">Please have someone call me (If you want someone to call you, check the radio button and add your phone: (999)999-9999</span>
				</div>
				<div class="answer">
					<span class="ans-text"><?php  echo $new['phone_bad']['contact'] ?>&nbsp;:&nbsp;<?php echo $new['phone_bad']['number'] ?></span>
				</div>
			</div>
		</div>
		<div class="wrap" id="pg_fourteen">
			<div class="intro-wrap">
				<h1 class="intro">14. Literature</h1>
			</div>
			<div class="answer-wrap">
				<div class="input">
					<span class="label">Please comment on our trip preparation and marketing materials</span>
				</div>
				<div class="answer">
					<span class="ans-table">
						<table>
							<tr>
								<th>Literature</th><th>Rating</th>
							</tr>
						<?php   
							foreach ( $new['marketing'] as $lit => $rating ) { 
						?>
							<tr>
								<td><?php echo $lit ?></td><td><?php echo $rating ?></td>
							</tr>		
						<?php	
							}
						?>
						</table>
					</span>
				</div>
			</div>
		</div>
	</body>
</html>