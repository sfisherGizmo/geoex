<?php

class BeaconPull {

	public $url_base = "https://data2011.peak15systems.com/beacon2011/service.svc/get/geoex/entity/contact";
	public $creds = "Alice.howell@tripsync:alice@GX";
	public $email;
	public $name; 

	public function __construct( $email, $name ) {
		$this->email = $email;
		$this->name = $name;
		$this->creds = $this->creds;
		$this->url_base = $this->url_base . '?emailaddress1=' . $this->email; 
	}

	public function getRecord() {
		
		$ch = curl_init( $this->url_base );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_USERPWD, $this->creds );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$xml = curl_exec( $ch );
		curl_close($ch);
		
		return $xml;	
	}

}

class BeaconInsert {

	public $url_base = "https://data2011.peak15systems.com/beacon2011/service.svc/insert/complex/evaluation";
	public $token = "5IU3woE0law7uprlac6iuTrIu7o3hiu35iuqiesWlAyLeJ64sLavl0xlan8exlas";
	public $contact_id;
	public $name; 
	public $trip_name;
	public $booking_id;
	public $sg_url;
	public $args;

	public function __construct( $contact_id, $booking_id, $trip_name, $sg_url ) {
		
		$this->contact_id = $contact_id;
		$this->booking_id = $booking_id;
		$this->trip_name = $trip_name;
		$this->sg_url = $sg_url;
		$this->url_base = $this->url_base . '?token='. $this->token . '&p15_evaluation.p15_guestid=' . $this->contact_id . '&p15_evaluation.p15_bookingid=' . $this->booking_id . '&p15_evaluation.p15_comments=' . $this->trip_name . '&p15_evaluation.p15_suggestions=' . $this->sg_url;
	}

	public function upsert() {
		//var_dump($this->url_base);
		$ch = curl_init( $this->url_base );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$xml = curl_exec( $ch );
		curl_close($ch);

		return $xml;
	}
}


?>