<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

class Pull {

	protected static $user = 'stephen@squibble-fish.com';
	protected static $md5 = 'b1c7e8fcb990fc07c25194f6eacd8903';
	protected static $sid = '1161513';
	protected static $v = 'v4';
	

	public function get() {

		require_once dirname( dirname( __FILE__ ) ) . '/sgapiModel.php';
		
		$today_date = date('Y-m-d+H:i:s' , strtotime('03:00:00 - 1 day') );
		$prev_date = date('Y-m-d+H:i:s' , strtotime('03:00:00 - 2 day') );
		// $today_date = '2015-08-11+03:00:00';
		// $prev_date = '2015-08-10+03:00:00';
		$sg_api = new restapi;
		
		$ids = array(
			'survey' => self::$sid,
			'surveyresponse' => "",
		);

		$filters = array( 
			array( 'datesubmitted', '>=', $prev_date ),
			array( 'datesubmitted', '<=', $today_date ),
		);

		$sg_api->setup( self::$user, self::$md5, self::$v, $resultsperpage=100 );

		$filter = $sg_api->setFilter($filters);
		
		$response = $sg_api->get( "surveyresponse", $ids );

		return $response;
	}
} 

$n = new Pull;
$sg = $n->get();

if ( empty( $sg ) ) {
	die( "No data for the time frame" );
}

require_once "beacon.php";

$daily = array();

foreach ( $sg as $response ) {

	$p = new BeaconPull( $response->{'[question(122)]'}, $response->{'[question(87)]'} );

	$xml = $p->getRecord();
	$d = new DOMDocument();
	$dom = $d->loadXML( $xml ); 
	$contacts = $d->getElementsByTagName( 'contactid' );
	
	foreach ( $contacts as $c ) {
		
		$daily[ $response->{'id'} ] = array(
			'contactid' => substr( $c->nodeValue, 1, -1 ),
			'id' 		=> $response->{'id'},
			'email' 	=> $response->{'[question(122)]'},
			'name' 		=> $response->{'[question(87)]'},
			'booking'	=> $response->{'[question(206)]'}, 
			'trip'		=> $response->{'[question(138)]'},
			'sg_url'	=> urlencode( "https://".$_SERVER['HTTP_HOST'].'/sg-tools/results/result.php?id='.$response->{'id'} ),
		);		
	}
}

foreach ( $daily as $record ) {
	
	$u = new BeaconInsert( $record['contactid'], $record['booking'], $record['trip'], $record['sg_url'] );
	$add = $u->upsert();
	var_dump($u);
}

// echo "<pre>" . print_r( $daily, true ) . "</pre>";

?>


